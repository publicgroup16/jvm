### JVM demo for build of Gradle/Springboot, build of Dockerfile with Registry and deploy using Helm to Kubernetes

Description: The Springboot demo is just how to put the jvm options in kubernetes configmap and secret.

Note: Gitlab CI/CD and GCP/GKE were used to test.

Instruction on integrating GCP with Gitlab:
```
-create an account with GCP, create GKE cluster-1 
-on your gitlab repo, connect infrastructure/google-cloud first and point to your project
-connect your infrastructure/kubernetes-clusters, search your cluster name
-run below to enable agent on your gke
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install cluster-1 gitlab/gitlab-agent \
    --namespace gitlab-agent-cluster-1 \
    --create-namespace \
    --set image.tag=v15.7.0 \
    --set config.token=xxxxxxxxxxxxxxxxxxxxxxxxxxxx \
    --set config.kasAddress=wss://kas.gitlab.com 

- verify connection status on your infrastructure/kubernetes-clusters   
		
#delete
helm delete cluster-1 --namespace gitlab-agent-cluster-1

-copy .kube from your home folder and put it on the repo, not needed???

-run gitlab-runner on your gke below,

helm repo add gitlab https://charts.gitlab.io
kubectl create ns gitlab-runner
helm install -n gitlab-runner gitlab-runner -f runner.yaml gitlab/gitlab-runner --create-namespace

-get your token from your repo from runner...
#runner.yaml
replicas: 1
gitlabUrl: https://gitlab.com/
runnerRegistrationToken: "xxxxxxxxxxxxxxxxxxx"
concurrent: 10
logLevel: info
logFormat: json
rbac:
 create: true
namespace: gitlab-runner
podLabels: { run: gitlab-ci }
runners:
 privileged: true
 name: test-runner
 tags: test-runner
 
-verify your configuration 
disable share runners
uncheck protected
verify specific runner is running 

-delete
helm delete gitlab-runner -n gitlab-runner 

-run the rbac yaml
kubectl apply -f rbac.yaml
kubectl apply -f cbinding.yaml

#rbac
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: gitlab-runner
  namespace: gitlab-runner
rules:
  - apiGroups: ["","extensions", "apps"]
    resources: ["secrets","configmaps","pods","pods/attach","services","namespaces","deployments","replicasets","pods/portforward"]
    verbs: ["get","list","watch","create","update","patch","delete"]
  - apiGroups: ["","extensions", "apps"]
    resources: ["pods/exec"]
    verbs: ["create","patch","delete"]

#cbinding
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-runner-cbinding
subjects:
- kind: ServiceAccount
  name: default
  namespace: gitlab-runner
roleRef:
  kind: ClusterRole
  name: gitlab-runner
  apiGroup: rbac.authorization.k8s.io      
```
```
output:
$ curl xxxxxxxxx:8080/web
Configmap: tmpuser ----------- Secret: mysecret
```
